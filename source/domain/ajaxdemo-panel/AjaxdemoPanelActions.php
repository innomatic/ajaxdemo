<?php
/**
 * Innomatic
 *
 * LICENSE 
 * 
 * This source file is subject to the new BSD license that is bundled 
 * with this package in the file LICENSE.
 *
 * @copyright  1999-2012 Innoteam S.r.l.
 * @license    http://www.innomatic.org/license/   BSD License
 * @link       http://www.innomatic.org
 * @since      Class available since Release 5.0
*/

require_once('innomatic/desktop/panel/PanelActions.php');

class AjaxdemoPanelActions extends PanelActions
{
    private $_localeCatalog;
    public $status;

    public function __construct(PanelController $controller)
    {
        parent::__construct($controller);
    }

    public function beginHelper()
    {
        require_once('innomatic/locale/LocaleCatalog.php');
        $this->_localeCatalog = new LocaleCatalog(
            'innomatic::root_domains',
            InnomaticContainer::instance('innomaticcontainer')->getLanguage()
        );
    }

    public function endHelper()
    {
    }
    
    public function ajaxClearHelloWorld() {
    	$objResponse = new XajaxResponse();
    	$objResponse->addAssign("mytext", "innerHTML", '');
    	$objResponse->addAssign("mywui", "innerHTML", '');
    	 
    	return $objResponse;
    }
    
    public function ajaxHelloWorld() {
    	$objResponse = new XajaxResponse();        
    	$objResponse->addAssign("mytext", "innerHTML", 'Hello World!');
    
    	return $objResponse;
    }
    
    public function ajaxWuiHelloWorld() {
    	//WuiXml::getContentFromXml('', '');
    	$objResponse = new XajaxResponse();
    
    	$xml = '<vertframe><children>
    			<label><args><label>prova wui xml</label></args></label>
    			<horizbar/>
    			<button><name>clear</name>
    			  <args>
    			    <label>Clear</label>
            <horiz>true</horiz>
            <frame>false</frame>
    			<action>javascript:void(0)</action>
            <themeimage>button_ok</themeimage>
    			</args>
    			  <events>
    			    <click>xajax_ClearHelloWorld()</click>
    			  </events>
    			</button>
    			</children>
    			</vertframe>';
    	$html = WuiXml::getContentFromXml('', $xml);
    
    	$objResponse->addAssign("mywui", "innerHTML", $html);
    
    	return $objResponse;
    }
}
