<?php
/**
 * Innomatic
 *
 * LICENSE 
 * 
 * This source file is subject to the new BSD license that is bundled 
 * with this package in the file LICENSE.
 *
 * @copyright  1999-2012 Innoteam S.r.l.
 * @license    http://www.innomatic.org/license/   BSD License
 * @link       http://www.innomatic.org
 * @since      Class available since Release 5.0
*/

require_once('innomatic/desktop/panel/PanelViews.php');

class AjaxdemoPanelViews extends PanelViews
{
    public $wuiPage;
    public $wuiMainvertgroup;
    public $wuiMainframe;
    public $wuiMainstatus;
    public $wuiTitlebar;
    public $xmlDef;

    public function update($observable, $arg = '')
    {
        switch ($arg) {
            case 'status':
                $this->wuiMainstatus->mArgs['status'] =
                    $this->_controller->getAction()->status;
                break;
        }
    }

    public function beginHelper()
    {
        require_once('innomatic/wui/dispatch/WuiEventsCall.php');
        require_once('innomatic/wui/dispatch/WuiEvent.php');

        $this->_wuiContainer->loadAllWidgets();
        //$this->_wuiContainer->registerAjaxCall('xajax_helloworld');

        $this->wuiPage = new WuiPage('page', array('title' => 'Ajax demo'));
        $this->wuiMainvertgroup = new WuiVertGroup('mainvertgroup');
        $this->wuiTitlebar = new WuiTitleBar(
            'titlebar',
            array(
                'title' => 'Ajax Demo',
                'icon' => 'folder_home')
            );
        $this->wuiMainvertgroup->addChild($this->wuiTitlebar);

        // Main tool bar
        //
        $wuiMainToolBar = new WuiToolBar('maintoolbar');

        $homeAction = new WuiEventsCall();
        $homeAction->addEvent(new WuiEvent('view', 'default', ''));
        $wuiHomeButton = new WuiButton(
            'homebutton',
            array(
                'label' => 'Hello World',
                'themeimage' => 'folder_home',
                'horiz' => 'true', 'action' => $homeAction->getEventsCallString()
                ));
        $wuiMainToolBar->addChild($wuiHomeButton);


        // Toolbar frame
        //
        $wuiToolbarFrame = new WuiHorizGroup('toolbarframe');

        $wuiToolbarFrame->addChild($wuiMainToolBar);
        $wuiToolbarFrame->addChild($wuiSitToolbar);
        $wuiToolbarFrame->addChild($wuiHelpToolbar);
        $this->wuiMainvertgroup->addChild($wuiToolbarFrame);

        $this->wuiMainframe = new WuiHorizFrame('mainframe');
        $this->wuiMainstatus = new WuiStatusBar('mainstatusbar');
    }

    public function endHelper()
    {
        // Page render
        //
    	$this->wuiMainframe->addChild(new WuiXML('xml', array('definition' => $this->xmlDef)));
        $this->wuiMainvertgroup->addChild($this->wuiMainframe);
        $this->wuiMainvertgroup->addChild($this->wuiMainstatus);
        $this->wuiPage->addChild($this->wuiMainvertgroup);
        $this->_wuiContainer->addChild($this->wuiPage);
    }

    public function viewDefault($eventData)
    {    	
    	$this->xmlDef = '<horizgroup><children>
    			<vertgroup><children>
    			<button><name>helloworld</name>
    			  <args>
    			    <label>Hello world</label>
            		<horiz>true</horiz>
            		<frame>false</frame>
            		<themeimage>button_ok</themeimage>
    				<action>javascript:void(0)</action>
    			  </args>
    			  <events>
    			    <click>xajax_HelloWorld()</click>
    			  </events>
    			</button>
    			<divframe><args><id>mytext</id></args><children><void/></children></divframe>
    			</children></vertgroup>
    			
    			<vertgroup><children>
    			<button><name>wuihelloworld</name>
    			  <args>
    			    <label>WUI Hello world</label>
            <horiz>true</horiz>
            <frame>false</frame>
            <themeimage>button_ok</themeimage>
    			<action>javascript:void(0)</action>
    			  </args>
    			  <events>
    			    <click>xajax_WuiHelloWorld()</click>
    			  </events>
    			</button>
    			<divframe><name>mywui</name><args><id>mywui</id></args><children><void/></children></divframe>
    			</children></vertgroup>
    			
    			<vertgroup><children>
    			<label><args><label>Autocomplete</label></args></label>
    			<string><name>testac</name>
    			  <args>
    			    <id>ac</id>
    			    <disp>action</disp>
    			    <autocomplete>true</autocomplete>
    			    <autocompletesearchurl>'.WuiXml::cdata(WuiEventsCall::buildEventsCallString(
                    '',
                    array(array('view', 'searchstring'))
                )).'</autocompletesearchurl>
    			  </args>
    		    </string>
    			</children></vertgroup>
    			
    			</children></horizgroup>';
    }
    
    public function viewSearchstring($eventData) {
    	$root_da = InnomaticContainer::instance('innomaticcontainer')->getDataAccess();
    	 
    	$query = $root_da->execute('SELECT id, name, file FROM wui_widgets WHERE name LIKE "%'.$_GET['term'].'%"');
    	$k = 0;
    	
    	while (!$query->eof) {
    		$content[$k]['id'] = $query->getFields( 'id' );
    		$content[$k++]['value'] = $query->getFields( 'name' ).' ['.$query->getFields( 'file').']';
    		$query->moveNext();
    	}
    	echo json_encode($content);
    	InnomaticContainer::instance('innomaticcontainer')->halt();
    }
}
